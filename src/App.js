import React, { useState } from 'react';
import * as firebase from 'firebase/app';
import 'firebase/analytics';
import {
  Container,
  Box,
  FormControl,
  InputLabel,
  OutlinedInput,
  InputAdornment,
  IconButton,
  CircularProgress,
  Grid,
  Chip,
  Snackbar,
} from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import axios from 'axios';
import logo from './logo.png';
import './App.scss';

const firebaseConfig = {
  apiKey: process.env.REACT_APP_API_KEY,
  authDomain: process.env.REACT_APP_AUTH_DOMAIN,
  databaseURL: process.env.REACT_APP_DATABASE_URL,
  projectId: process.env.REACT_APP_PROJECT_ID,
  storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_SENDER_ID,
  appId: process.env.REACT_APP_APP_ID,
  measurementId: process.env.REACT_APP_MEASUREMENT_ID,
};

firebase.initializeApp(firebaseConfig);
firebase.analytics();

// eslint-disable-next-line no-extend-native
String.prototype.capitalize = function(lower) {
  return (lower ? this.toLowerCase() : this).replace(/(?:^|\s)\S/g, function(a) {
    return a.toUpperCase();
  });
};

export default () => {
  const [query, setQuery] = useState('01123.020');
  const [isLoading, setIsLoading] = useState(false);
  const [isSuccess, setIsSuccess] = useState(false);
  const [data, setData] = useState([]);
  const [open, setOpen] = useState(false);
  const [message, setMessage] = useState('');

  const ListBodegas = () => {
    const { localidades } = data;
    if (localidades) {
      return localidades.map(loc => (
        <Box key={loc.localidad}>
          <h2>{loc.localidad}</h2>
          <ul>
            {loc.bodegas.map(bod => {
              if (bod.bodega !== 'TRANSITO')
                return (
                  <li key={bod.bodega}>
                    {bod.bodega.capitalize(true)} <Chip label={bod.total} className="custom-chip" />
                  </li>
                );
              else return null;
            })}
          </ul>
        </Box>
      ));
    }
    return <CircularProgress color="secondary" size={25} />;
  };

  const ListTotales = () => {
    const { localidades } = data;
    if (localidades) {
      return localidades.map(loc => (
        <Box key={loc.localidad} className={loc.localidad.replace(/\s/g, '').toLowerCase()}>
          <h2>
            Total
            <br /> {loc.localidad}:
          </h2>
          <Chip label={loc.total} className="custom-chip" />
        </Box>
      ));
    }
    return <CircularProgress color="secondary" size={25} />;
  };

  const handleSubmit = e => {
    e.preventDefault();
    setIsLoading(true);

    const url =
      process.env.NODE_ENV === 'development'
        ? 'http://localhost:5001/flux-inventario/us-central1/api/getStock'
        : 'https://us-central1-flux-inventario.cloudfunctions.net/api/getStock';

    axios
      .get(url, {
        params: {
          itemId: query,
        },
      })
      .then(response => {
        // handle data
        if (response.data.producto === 'Producto no encontrado') {
          setIsSuccess(false);
          setIsLoading(false);
          setMessage('Producto no encontrado');
          setOpen(true);
        } else {
          setIsSuccess(true);
          setIsLoading(false);
          setData(response.data);
        }
        console.log(response.data);
      })
      .catch(function(error) {
        // handle error
        setIsSuccess(false);
        setIsLoading(false);
        setMessage(error.message);
        setOpen(true);
        console.log(error);
      });
  };

  return (
    <div className="app">
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        open={open}
        autoHideDuration={3000}
        onClose={() => setOpen(false)}
        message={message}
      />

      <Box className="header">
        <img src={logo} alt="Logo" className="logo" />
      </Box>
      <Box className="header-bottom">test</Box>
      <Container className="content" maxWidth="sm">
        <Box className="search-component">
          <h1>Introduce el codigo del producto:</h1>
          <form onSubmit={handleSubmit}>
            <FormControl variant="outlined" className="search-input">
              <InputLabel htmlFor="search" color="secondary">
                Codigo:
              </InputLabel>
              <OutlinedInput
                id="search"
                color="secondary"
                fullWidth
                required
                endAdornment={
                  <InputAdornment position="end">
                    {isLoading ? (
                      <CircularProgress color="secondary" size={25} />
                    ) : (
                      <IconButton type="submit">
                        <SearchIcon />
                      </IconButton>
                    )}
                  </InputAdornment>
                }
                onChange={e => setQuery(e.target.value)}
                value={query}
                labelWidth={70}
              />
            </FormControl>
          </form>
        </Box>
      </Container>
      <Container maxWidth="sm">
        {isSuccess && (
          <Box>
            <Grid container spacing={3}>
              <Grid item xs={6}>
                <ListBodegas />
              </Grid>
              <Grid item xs={6} className="totals">
                <ListTotales />
              </Grid>
            </Grid>
          </Box>
        )}
      </Container>
    </div>
  );
};
