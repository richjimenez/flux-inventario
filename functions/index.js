const functions = require('firebase-functions');
const express = require('express');
const cors = require('cors');
const axios = require('axios');

const app = express();

app.use(cors({ origin: true }));

app.get('/getStock', (req, res) => {
  const url = 'http://fluxcorpo2.dyndns.org:8080/webservices/ws/stocks/itemStock';
  axios
    .get(url, {
      params: {
        itemId: req.query.itemId,
      },
    })
    .then(response => {
      res.json(response.data);
      return res;
    })
    .catch(error => {
      console.log(error);
      res.status(500).send('Ocurrio un error al consultar la base de datos.');
    });
});

exports.api = functions.https.onRequest(app);

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
